import tensorflow as tf
import numpy as np
import string, random

# Load data.
encX = np.load("encX.npy")
decX = np.load("decX.npy")
decy = np.load("decy.npy")

epochs = 10000
learningRate = 0.01
hiddenLayerSize = 256
exampleCount = encX.shape[0]

# Define alphabet. We're going to use this on decoding the sequence.
alphabet = " \t\n" + string.ascii_lowercase + string.ascii_uppercase
alphabetSize = len(alphabet)

# This tensors represents the input of the encoder, decoder and target of the decoder.
enc_xx_n = tf.placeholder(tf.int32, shape=[None, None])
dec_xx_n = tf.placeholder(tf.int32, shape=[None, None])
dec_yy_n = tf.placeholder(tf.int32, shape=[None, None])

enc_xx = tf.one_hot(enc_xx_n, alphabetSize)
dec_xx = tf.one_hot(dec_xx_n, alphabetSize)
dec_yy = tf.one_hot(dec_yy_n, alphabetSize)

# BasicLSTMCell will be used as RNN cell.
lstm_cell = tf.nn.rnn_cell.BasicLSTMCell(hiddenLayerSize)

# Encoder. Takes enc_xx tensor as input.
# We define e_out but we will simply ignore it.
e_out, e_state = tf.nn.dynamic_rnn(lstm_cell, enc_xx, dtype=tf.float32)

# Decoder. Takes dec_xx tensor as input, and initial state is the same as encoder's last state.
d_out, d_state = tf.nn.dynamic_rnn(lstm_cell, dec_xx, initial_state=e_state, dtype=tf.float32)

# Output of the decoder network.
logits = tf.layers.dense(d_out, units=alphabetSize)

# This tensor will be used for testing the network.
# During training, Softmax is already calculated by loss (softmax_cross_entropy).
pred = tf.nn.softmax(logits)

# Takes the output of the network WITHOUT Softmax. Applies it Softmax automatically and calculates loss.
loss = tf.losses.softmax_cross_entropy(logits=logits, onehot_labels=dec_yy)

optimizer = tf.train.AdamOptimizer(learning_rate=learningRate, beta1=0.9, beta2=0.999, epsilon=1e-08)
train = optimizer.minimize(loss)

sess = tf.Session()
sess.run(tf.global_variables_initializer())

# Builds an one-hot encoded vector and returns it.
def oneHotVector(index, size):
	r = np.zeros((size))
	r[index] = 1
	return r

# Decodes sequence for given matrix. Parameter should have (Timestep, features) shape.
def decodeSeq(x):
	text = ""

	# For each timstep.
	for t in range(0, x.shape[0]):
		# Find the highest value on vector, and give it's index.
		ind = np.argmax(x[t])

		# And take back the character from alphabet.
		text += alphabet[ind]
	text = text.strip()
	return text

# Tests random example from training set.
def testRandomExample():
	ri = random.randint(0, exampleCount-1)

	b_encX = encX[ri]
	b_decX = decX[ri]
	b_decy = decy[ri]

	# Reshape it.
	b_encX = b_encX.reshape((1,) + b_encX.shape)
	b_decX = b_decX.reshape((1,) + b_decX.shape)
	b_decy = b_decy.reshape((1,) + b_decy.shape)

	feed = {
		enc_xx_n:b_encX,
		dec_xx_n:b_decX,
		dec_yy_n:b_decy
	}

	b_encX_onehot, b_decX_onehot, b_decy_onehot = sess.run([enc_xx, dec_xx, dec_yy], feed_dict=feed)
	prediction = sess.run(pred, feed_dict=feed)

	print("Encoder input:", decodeSeq(b_encX_onehot[0]))
	print("Decoder target:", decodeSeq(b_decy_onehot[0]))
	print("Model's output:", decodeSeq(prediction[0]))
	print("--------------------------------")

# Train!
dataIndex = 0
for e in range(0, epochs):
	b_encX = encX[dataIndex]
	b_decX = decX[dataIndex]
	b_decy = decy[dataIndex]

	# Matrices has (Timestep, Feature) shape.
	# But our model needs input as (Batch, Timestep, Feature).
	
	# So we reshape it.
	b_encX = b_encX.reshape((1,) + b_encX.shape)
	b_decX = b_decX.reshape((1,) + b_decX.shape)
	b_decy = b_decy.reshape((1,) + b_decy.shape)

	# Next example.
	dataIndex += 1
	# We went through all the dataset? Reset it.
	if dataIndex >= exampleCount:
		dataIndex = 0

	feed = {
		enc_xx_n:b_encX,
		dec_xx_n:b_decX,
		dec_yy_n:b_decy
	}

	sess.run(train, feed)

	# At every %1 progress, print current batch's loss, and test one example.
	if (e%(epochs/100)) == 0:
		print("Epoch:", int(e), "/", int(epochs))
		print("Batch loss:", sess.run(loss, feed))
		testRandomExample()
