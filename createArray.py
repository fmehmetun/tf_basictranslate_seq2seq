#! -*- coding: UTF-8 -*-
import numpy as np
import re, string

# Applies filter to given list. (Deletes empty elements from list.)
def applyFilter(x):
	return list(filter(None, x))

# Word filter. Deletes unwanted char from given string.
def wordFilter(x):
	# Remove Turkish alphabet characters from string.
	x = x.replace("Ğ", "G").replace("Ö", "O").replace("Ç", "C").replace("İ", "I").replace("Ü", "U").replace("Ş", "S")
	x = x.replace("ğ", "g").replace("ö", "o").replace("ç", "c").replace("ı", "i").replace("ü", "u").replace("ş", "s")

	# String only can have lowercase letters, uppercase letters, tab, newline and space characters.
	x = re.sub(r"""[^A-Za-z\t\n ]""", "", x)

	return x

# Builds an one-hot encoded vector and returns it.
def oneHotVector(index, size):
	r = np.zeros((size))
	r[index] = 1
	return r

# Read our dataset from text file and split it.
data = applyFilter(open("data.txt", "r").read().split("\n"))

# Our 'vocabulary'. We are going to process data character-by-character.
alphabet = " \t\n" + string.ascii_lowercase + string.ascii_uppercase
alphabetSize = len(alphabet)

# encX: Input to encoding network.
# decX: Input to decoding network.
# encX: Target output of the decoding network.
encX = []
decX = []
decy = []

# Process every example and store them as one-hot encoded matrices.
for d in data:
	enc_xx = []
	dec_xx = []
	dec_yy = []

	en = wordFilter(d.split("\t")[0])
	tr = wordFilter("\t" + d.split("\t")[1] + "\n")

	# In encoder network, we feed the source language's example character-by-character, and simply ignore the output.
	# So there is no target output for the encoder network.
	# In the end of sentence, we will keep LSTM network's state.
	for i, e in enumerate(en):
		enc_xx.append(alphabet.index(e))

	# In decoder network, we feed a character, and we hope network is going to predict next character.
	for i in range(len(tr)):
		if i < len(tr)-1:
			dec_xx.append(alphabet.index(tr[i]))
			dec_yy.append(alphabet.index(tr[i+1]))

	if len(enc_xx) > 1 and len(dec_xx) > 1 and len(dec_yy) > 1:
		# Convert list to matrix.
		enc_xx = np.array(enc_xx)
		dec_xx = np.array(dec_xx)
		dec_yy = np.array(dec_yy)

		encX.append(enc_xx)
		decX.append(dec_xx)
		decy.append(dec_yy)

		print("EN:", en.strip())
		print("TR:", tr.strip())
		print("-----------------")

# We appended all the examples' input and target values as one-hot encoded vector.
# And timestep depends on how many characters that example have.
# Numpy will store all of the matrices in a list of matrices.
encX = np.array(encX)
decX = np.array(decX)
decy = np.array(decy)

print(encX[0].shape)
print(decX[0].shape)
print(decy[0].shape)

# Save the matrices.
np.save("encX", encX)
np.save("decX", decX)
np.save("decy", decy)